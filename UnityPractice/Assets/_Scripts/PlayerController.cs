﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    Rigidbody rb;

    public float speed;

    public GameObject stage;
    public GameObject currentStage;

    int count = 0;

    public Text countText;
    public Text winText;
    public Text encourageText;

    Vector3 stageSpawnPoint = new Vector3();

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        stageSpawnPoint = new Vector3(0, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate() {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        rb.AddForce(movement * speed);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            count += 1;

            SetCountText();
            Destroy(other.gameObject);
        }
    }
    void SetCountText()
    {
        countText.text = "Count: " + count;

        if (count >= 9)
        {
            winText.text = "U when.";
        }
        switch (count)
        {
            case 1:
                encourageText.text = "....hey";
                break;
            case 3:
                encourageText.text = "hey";
                break;
            case 5:
                encourageText.text = "hey, listen";
                break;
            case 6:
                encourageText.text = "hey, LISTEN";
                break;
            case 7:
                encourageText.text = "LINK";
                break;
            case 8:
                encourageText.text = "LINK, LISTEN!!!";
                break;
            case 9:
                //Destroy(encourageText.gameObject);
                SpawnStage();
                count = 0;
                break;
        }
    }
    void SpawnStage()
    {
        stageSpawnPoint.Set(stageSpawnPoint.x, stageSpawnPoint.y - 20, stageSpawnPoint.z);
        print("heyya");
        Destroy(currentStage);
        currentStage = Instantiate(stage, stageSpawnPoint, Quaternion.identity);
    }
}
